from dataclasses import dataclass
import csv
import sys

@dataclass
class SentimentReview:
    text: str
    score: int

    def __str__(self):
        return self.text + " " + str( self.score)

@dataclass
class Review:
    game_id: int
    game_name: str
    text: str
    score: int
    votes: int

def text_preprocessing(line):
    """
        Function which removes all unneccessary characters from
        a review (e.g. commas, apostrophes, semi-colons, colons etc.)
        
        params:
            line(str)
            
        returns:
            preproccesed line(str)
    """
    
    pass

def main(filename):
    try:
        reviews = []
        dataset_size = 200
        dataset_i = 0
        
        file = open(filename, 'r', encoding = "utf-8", errors = "ignore")
        next(file)
        reader_obj = csv.reader( file)
        
        for review in reader_obj:
            if dataset_i > dataset_size:
                break
            
            text = review[2]
            score = review[-2]

            score = 0 if score == -1 else score

            reviews.append( SentimentReview( text, score))

            dataset_i += 1

        for review in reviews:
            print(review)
        
        file.close()

    except FileNotFoundError:
        print("Dataset file could not be found at specified path.")
        exit(-1)

if __name__ == "__main__":
    filename = sys.argv[1] if len(sys.argv) != 1 else input("Dataset filename > ")
    main( filename)
